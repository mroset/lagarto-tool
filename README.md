# Lagarto Lowrisc Tool

This tool is designed to automate the most common experiments performed with the Lagarto Lowrisc repository.

## Features

- Collect PMU output into a `.csv` file.
- Run as many executables in parallel as desired with `-j`
- Easy interface with ./DefaultConfig-sim with `--flags`. Currently only supporting torture dump.
- Detect end of EEMBC tests (look for `Non-Intrusive CRC` text)

## Installation

Install 3rd party modules with the following command:

```bash
$ python3 -m pip install -r requirements.txt
```

## Usage

Check usage with the classical flag...
```bash
$ python3 lltool.py -h
```

**Note:** To use the tool from everywhere consider adding a link to a folder present in `$PATH` like:
```bash
ln -s /home/bscuser/.local/opt/lltool/lltool.py /home/bscuser/.local/bin/lltool
```
## Examples

- Execute all RISCV benchmarks and collect all PMU metrics in a .csv format:

```bash
$ python3 lltool.py -r /path/to/lagarto-lowrisc    \
                    -o /path/to/output/directory   \ 
                    --riscv_benchmarks
```

- Execute all `*.riscv` of a directory with torture dump.

```bash
$ python3 lltool.py -r /path/to/lagarto-lowrisc   \
                    -o /path/to/output/directory  \
                    --exec_from_dir /path/to/dir  \
                    --flags t 
```
