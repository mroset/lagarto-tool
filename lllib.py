# Std libraries
import csv
import logging
import signal

from concurrent.futures import Future
from concurrent.futures import ThreadPoolExecutor
from concurrent.futures import as_completed
from concurrent.futures import as_completed

from typing             import Dict, List
from pathlib            import Path

# Lagarto-Tool imports
from llsimrunner import LLSimRunner, FinishReason, DefaultConfigSimFlags
from llrepo      import LLRepo




def run_simulations(llrepo                   : LLRepo,
                    executables              : List[Path],
                    output_path              : Path,
                    max_workers              : int,
                    default_config_sim_flags : List[DefaultConfigSimFlags]):
    """ Run and gather results of ./vsim/DefaultConfig-sim."""

    futures_to_runners : Dict[Future, LLSimRunner] = {}

    pmu_summary_csv_path    = Path(output_path, Path('pmu_summary.csv'))
    pmu_summary_csv_headers = ['Benchmark'] + LLSimRunner.PMU_METRIC_NAMES

    # Handle signal to shutdown ThreadPoolExecutor
    def signal_handler(signal, frame):
        logging.critical(f'Received signal... {repr(signal)}')
        logging.critical("Cancelling pending tasks...")

        # Cancel pending tasks
        for future in futures_to_runners.keys():
            future.cancel()

    signal.signal(signal.SIGINT, signal_handler)

    # Prepare summary...
    with open(pmu_summary_csv_path, 'w') as report_csv_file:
        writer = csv.DictWriter(report_csv_file, fieldnames = pmu_summary_csv_headers)
        writer.writeheader()
    
    msg = "Executing {} simulations with {} processes".format(len(executables), max_workers)
    logging.info(msg)

    with ThreadPoolExecutor(max_workers) as executor:
        # Launch tasks...
        for executable in executables:
            runner = LLSimRunner(llrepo, executable, output_path)
            runner.default_config_sim_flags = default_config_sim_flags
            future = executor.submit(runner.run)
            futures_to_runners[future] = runner

        # Gather finished tasks and update report...
        for future in as_completed(futures_to_runners):
            runner = futures_to_runners[future]
            future_finished_normally = future.done()      \
                               and not future.cancelled() \
                               and not future.exception()

            if future_finished_normally:
                finish_reason, pmu_metrics = future.result()
                if pmu_metrics:
                    with open(pmu_summary_csv_path, 'a') as report_csv_file:
                        writer = csv.DictWriter(report_csv_file, fieldnames = pmu_summary_csv_headers)
                        writer.writerow(pmu_metrics)
                
                if finish_reason == FinishReason.OK:
                    logging.info(f'OK... {runner.name}')

                elif finish_reason == FinishReason.STALL:
                    logging.error(f"Stall... {runner.name}")

                elif finish_reason == FinishReason.SIGINT:
                    logging.error(f"Killed... {runner.name}")

                elif finish_reason == FinishReason.ERROR:
                    logging.error(f"Non normal exit... {runner.name}.log: {runner.get_log_line_with_error_code()}")

            elif future.cancelled():
                logging.warning(f"Cancelled... {runner.name}")

            elif future.exception():
                msg = 'Exception... {}: {}'.format(runner._executable_path.stem, future.exception())
                logging.error(msg)
