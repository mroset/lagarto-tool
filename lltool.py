#!/usr/bin/env python3

# Std libraries
import argparse
import sys
import os
import logging

from pathlib import Path
from glob    import glob
from typing  import List

# 3rd party imports
from rich.logging import RichHandler

# Lagarto-Tool imports
import lllib

from llrepo      import LLRepo
from llsimrunner import DefaultConfigSimFlags

BANNER = \
'''                     ..
                       +*=-:.
                       .***+++=-:
               :---:::..+***+++++++=:
                .---------===++++++++++.
                  ---------------=++**+
                   -------:::::---****:
                    .::::::::-----+***
                      ::::::-------***.
                       .::---------=**-  -.
                        :------------+*. .%+=.
   -*----===   .#----=+:      =%-:----=*+#@@@#-
   -=      :*  .*      #     :*:*  .:=#@@%%##%%#=.
   -=       *. .*     -*     *  -+   *@@-    :**#
   -=       +: .*   +*:     *=:::*-  #@+
   -=       #  .*    +-    ==:::::*. =@+
   -=     :+:  .*     +-  :+      .*  =%-     .-
   .======:     -      -  -        ::   :::.::.
'''

PROGRAM_DESCRIPTION = BANNER + \
'''
Support tool to automate experiments with Lagarto Lowrisc
and the post execution data processing.  '''

if __name__ == "__main__":

    # Command line arguments
    # ----------------------------
    parser = argparse.ArgumentParser(description      =  PROGRAM_DESCRIPTION,
                                     formatter_class  =  argparse.RawTextHelpFormatter,
                                     add_help         =  False,
                                     epilog           =  'Report any bugs... :)')

    args_executables_help = '''What do you want to execute?'''

    args_required           = parser.add_argument_group('Required arguments')
    args_optional           = parser.add_argument_group('Optional arguments')
    args_executables        = parser.add_argument_group("RV executables", args_executables_help)
    args_defaultconfig_sim  = parser.add_argument_group('DefaultConfig-sim arguments')

    args_required.add_argument(
            '-r',
            '--repo',
            type       = Path,
            required   = True,
            help       = 'Directory path with the Lagarto Lowrisc repository.')

    args_required.add_argument(
            '-o',
            '--output',
            type      = Path,
            required  = True,
            metavar   = "DIR",
            help      = 'Choose a directory for all the output')

    args_optional.add_argument(
            "-h",
            "--help",
            action  = "help",
            help    = "show this help message and exit")

#    args_optional.add_argument(
#            '-B',
#            '--root_branch',
#            help = 'Change Lagarto Lowrisc branch to work with.')
#
#    args_optional.add_argument(
#            '-b',
#            '--core_branch',
#            type = str,
#            help = 'Change Lagarto Ka branch to work with.')

    args_optional.add_argument(
            '-j',
            '--jobs',
            default  = 1,
            metavar  = "JOBS",
            type     = int,
            help     = "Specifies the number of jobs to run simultaneously (Default 1).")

    args_optional.add_argument(
            '-d',
            '--debug',
            action   = "store_true",
            help     = "Increment verbosity with debug information.")

    args_executables.add_argument(
            '--riscv_benchmarks',
            action   = 'store_true',
            help     = "Execute all RISCV benchmarks and collect results in a " + \
                       ".csv file.")

    args_executables.add_argument(
            '-e',
            '--exec_from_dir',
            metavar  = 'DIR',
            type     = Path,
            help     = "Execute all '*.riscv' files from DIR.")

    args_executables.add_argument(
            '--eembc_benchmarks',
            metavar  = 'DIR',
            type     = Path,
            help     = "Not implemented. TBD: check how to compile them.")

    args_defaultconfig_sim.add_argument(
            '-f',
            '--flags',
            choices = [flag.value for flag in DefaultConfigSimFlags],
            default = [],
            help    = 'Extra ./DefaultConfig-sim flags. Allowed: (t)orture')


    # Program variables
    # ----------------------------
    llrepo          : LLRepo 
    executables     : List[Path] = []

    logger         = logging.getLogger(__name__)
    logging_format = "%(message)s"
    logging_level  = "INFO"
    args           = parser.parse_args()
    args.output    = args.output.expanduser().absolute()



    # Check program input
    # ---------------------------------

    # --debug
    if args.debug:
        logging_level = "NOTSET"

    logging.basicConfig(
        level    = logging_level,
        format   = logging_format,
        handlers = [RichHandler(show_path=False, markup=True)],
        datefmt  = '%Y-%m-%d %H:%M:%S'
    )

    # --repo
    try:
        llrepo = LLRepo(root = args.repo)
    except FileNotFoundError as e:
        logging.critical("Can't create repository object: " + repr(e))
        sys.exit(os.EX_OSFILE)

    # --output
    try:
        args.output = args.output.resolve(strict=True)
    except FileNotFoundError:
        logging.critical("Output is not a directory or does not exist.")
        sys.exit(os.EX_OSFILE)

    # --flags
    args.flags = [DefaultConfigSimFlags(flag) for flag in args.flags]



    # Print Repository information
    # ----------------------------------
    try:
        logging.info(f"Lagarto Lowrisc commit id: {llrepo.commit_id()}")
    except:
        pass
    try:
        logging.info(f"Lagarto Ka commit id: {llrepo.lagarto_ka_commit_id()}")
    except:
        pass


    # Gather executables
    # ----------------------------
    if args.riscv_benchmarks:
        executables.extend(llrepo.get_rv_bmarks_paths())

        if not executables:
            logging.critical("No '*.riscv' executables in "
                             + llrepo.riscv_benchmarks.as_posix()
                             + " directory.")
            sys.exit(os.EX_OSFILE)

    elif args.exec_from_dir:
        for path in glob(args.exec_from_dir.resolve().as_posix() + '/*.riscv'):
            executables.append(Path(path))

        if not executables:
            logging.critical("Directory does not contain any '*.riscv' file.")
            sys.exit(os.EX_OSFILE)

    logging.info("Executables gathered for simulation: " + str(len(executables)))



    # Run simulations
    # ------------------
    lllib.run_simulations(llrepo,
            executables,
            args.output,
            args.jobs,
            args.flags)

