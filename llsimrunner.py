# Standard Library
import time
import subprocess
import re
import logging

from pathlib import Path
from enum    import Enum, auto
from typing import Tuple, Dict, List

# Lagarto-Lowrisc tool
from llrepo import LLRepo



class FinishReason(Enum):
    OK        = 0
    STALL     = 2
    SIGINT    = -2
    ERROR     = 255

class DefaultConfigSimFlags(Enum):
    TORTURE = 't'

class LLSimRunner:
    """Class to handle ./DefaultConfig-sim processes.
    
    Runs a simulation in a new process. It detects if the 
    simulation ends normally, ends in an EEMBC way or stalls.
    It also returns the PMU metrics if present in the output.
    """

    PMU_METRIC_NAMES = ['pmu  cycles',
                        'pmu  instructions',
                        'pmu  inst cache requests',
                        'pmu  inst cache miss (events)',
                        'pmu  inst cache miss (cycles)',
                        'pmu  inst cache kill (events)',
                        'pmu  inst cache kill (cycles)',
                        'pmu  inst cache bussy (cycles)',
                        'pmu  inst tlb miss (cycles)',
                        'pmu  data load requests',
                        'pmu  data store requests',
                        'pmu  data cache miss (events)',
                        'pmu  data tlb miss (cycles)',
                        'pmu  branch (events)',
                        'pmu  branch taken (events)',
                        'pmu  branch misspred (events)']

    def __init__(self,
                 llrepo           : LLRepo,
                 executable_path  : Path,
                 output_dir_path  : Path):

        self._executable_path    :  Path
        self._llrepo             :  LLRepo
        self._out_dir_path       :  Path
        self._out_file_path      :  Path
        self._popen              :  subprocess.Popen
        self._torture_file_path  :  Path
        self._torture_file_size  :  int

        self._llrepo                   =  llrepo
        self.out_dir_path              =  output_dir_path
        self.executable_path           =  executable_path
        self.default_config_sim_flags  =  []



    # Class properties
    # ----------------------

    @property
    def name(self):
        return self.executable_path.stem

    @property
    def out_dir_path(self):
        return self._out_dir_path

    @out_dir_path.setter
    def out_dir_path(self, path : Path):
        if not path.exists():
            raise ValueError("Output path does not exist.")
        else:
            self._out_dir_path = path.expanduser().absolute()

    @property
    def executable_path(self):
        return self._executable_path

    @executable_path.setter
    def executable_path(self, path: Path):
        if not path.exists():
            raise ValueError(f'{path.as_posix()} does not exist.')
        else:
            self._executable_path = path

    @property
    def default_config_sim_flags(self):
        return self._default_config_sim_flags

    @default_config_sim_flags.setter
    def default_config_sim_flags(self, args : List[DefaultConfigSimFlags]):
        self._default_config_sim_flags = args


    # Main Methods
    # ---------------------------

    def run(self) -> Tuple[FinishReason, Dict[str, str]]:
        """Run ./DefaultConfig-sim and gather PMU metrics"""

        finish_reason = None

        # Prepare command...
        out_file_name       =  Path(self._executable_path.stem + ".log")
        self._out_file_path =  Path(self._out_dir_path, out_file_name)
        args = [
                '+bootrom='           +  self._llrepo.bootrom_path.as_posix(),
                '+load='              +  self._executable_path.as_posix(),
                '+max-cycles=30000000']

        # Prepare ./DefaultConfig-sim flags
        if DefaultConfigSimFlags.TORTURE in self.default_config_sim_flags:
            torture_file_name = Path(f'{self._executable_path.stem}.vsim.sig.dirty')

            self._torture_file_path = Path(self._out_dir_path, torture_file_name)
            self._torture_file_path.open('w').close()
            self._torture_file_size = self._torture_file_path.stat().st_size

            args.extend([f'+torture_dump={self._torture_file_path.as_posix()}',
                           '+torture_dump_ON'])

        command = self._llrepo.defaultconfig_sim.as_posix() + ' ' + ' '.join(args)

        # Write command used...
        logging.debug(f'Starting... {self.name}')
        with self._out_file_path.open('w') as out_file: 
            out_file.write(command + '\n')

        # Launch command...
        popen =  subprocess.Popen(executable  =  self._llrepo.defaultconfig_sim,
                                  args        =  args,
                                  cwd         =  self._llrepo.vsim.as_posix(),
                                  stderr      =  subprocess.STDOUT,
                                  stdout      =  self._out_file_path.open('a'))

        # This should be done inside ./DefaultConfig-sim
        exit_code = popen.poll()
        while exit_code is None:
            time.sleep(3)
            if self.out_file_has_crc_instr():
                finish_reason = FinishReason.OK
                popen.kill()

            exit_code = popen.poll()

        logging.debug(f"./DefaultConfig-sim... {self.name}.riscv (exit_code: {exit_code})")
             
        # Handdle exit code of ./DefaultConfig-sim
        if finish_reason is None:
            if exit_code == FinishReason.SIGINT.value:
                finish_reason = FinishReason.SIGINT

            elif exit_code == FinishReason.OK.value:
                finish_reason = FinishReason.OK

            else:
                finish_reason = FinishReason.ERROR

        logging.debug(f'Finish reason... {self.name}: {finish_reason}')

        # Post processing of ./DefaultConfig-sim
        if DefaultConfigSimFlags.TORTURE in self.default_config_sim_flags:
            self.clean_torture_dump()

        return finish_reason, self.get_pmu_metrics()



    def clean_torture_dump(self) -> bool:
        """Parse torture dump with spike-dasm."""
        cleaned = False
        if self._torture_file_path.exists():
            torture_file_clean_name = Path(f'{self._executable_path.stem}.vsim.sig')
            torture_file_clean_path = Path(self._out_dir_path, torture_file_clean_name)
            try:
                subprocess.run('spike-dasm',
                                stdin  = self._torture_file_path.open('r'),
                                stdout = torture_file_clean_path.open('w'))

                self._torture_file_path.unlink()
                self._torture_file_path = torture_file_clean_path
                cleaned = True
            except Exception as e:
                msg = f"Cleaning torture dump of {self.name}: {str(e)}"
                logging.error(msg)

        return cleaned



    def out_file_has_crc_instr(self) -> bool:
        """EEMBC benchamrks way to check if finished"""
        with open(self._out_file_path) as out_file:
            if 'Non-Intrusive CRC' in out_file.read():
                return True
            else:
                return False



    def stalled(self) -> bool:
        """Check the torture dump file size to see if the proc. has stalled"""

        file_size_current = self._torture_file_path.stat().st_size
        if file_size_current == self._torture_file_size:
            return True
        else:
            self._torture_file_size = file_size_current
            return False

    

    def get_log_line_with_error_code(self) -> str:
        log_line_with_error_code = ''
        with open(self._out_file_path.as_posix(), 'r+') as out_file:
            for line in out_file:
                if re.search('exit with error code', line):
                    log_line_with_error_code = line.strip()
                    break
        return log_line_with_error_code



    def get_pmu_metrics(self) -> Dict[str, str]:
        """Parse ./DefaultConfig-sim output and parse PMU metrics"""

        pmu_metrics = {}
        pmu_metrics["Benchmark"] = self._out_file_path.stem

        with open(self._out_file_path.as_posix(), 'r') as out_file:
            out_file.seek(0,0)

            log_lines   = out_file.readlines()
            for line in log_lines:
                for metric_name in self.PMU_METRIC_NAMES:
                    if metric_name in line:
                        metric_value = re.search('\d+', line)
                        if metric_value:
                            pmu_metrics[metric_name] = metric_value.group(0)
                        else:
                            pmu_metrics[metric_name] = '-'
        return pmu_metrics
