# Std libraries
import os
import subprocess
import typing

from pathlib import Path 
from  glob   import  glob

# 3rd party imports
from git.repo import Repo
from git.exc  import InvalidGitRepositoryError



class LLRepo:

    # Paths from Lagarto-Lowrisc repository
    # -----------------------------------------------

    BOOTROM             =  Path('vsim/bootrom.hex')
    DEFAULT_CONFIG_SIM  =  Path('vsim/DefaultConfig-sim')
    LAGARTO_KA_CORE     =  Path('soc/submodules/asic_top/submodules/processor/' + \
                                'submodules/tile/submodules/lagarto_ka_core')
    RISCV_BENCHMARKS    =  Path('riscv-tools/riscv-tests/benchmarks')
    RISCV_ISA           =  Path('riscv-tools/riscv-tests/isa')
    VSIM                =  Path('vsim')

    def __init__(self, root: Path):
        self.root = root

    @property
    def root(self):
        return self._root

    @root.setter
    def root(self, path : Path):
        self._root = path.resolve(strict=True)

    @property
    def vsim(self):
        return Path(self.root, self.VSIM).resolve(strict=True)

    @property
    def bootrom_path(self):
        return Path(self.root, self.BOOTROM).resolve(strict=True)

    @property
    def defaultconfig_sim(self):
        return Path(self.root, self.DEFAULT_CONFIG_SIM).resolve(strict=True)

    @property
    def riscv_benchmarks(self):
        return Path(self.root, self.RISCV_BENCHMARKS).resolve(strict=True)

    @property
    def lagarto_ka(self):
        return Path(self.root, self.LAGARTO_KA_CORE).resolve(strict=True)

    def get_rv_bmarks_paths(self) -> typing.List[Path]:
       return [Path(path) for path in glob(self.riscv_benchmarks.as_posix() + '/*.riscv')]


    # Fancy stuff...
    # -----------------
    def lagarto_ka_has_git(self) -> bool:
        try:
            Repo(self.lagarto_ka)
        except InvalidGitRepositoryError:
            return False
        else:
            return True

    def lagarto_lowrisc_has_git(self) -> bool:
        try:
            Repo(self.root)
        except InvalidGitRepositoryError:
            return False
        else:
            return True

    def lagarto_ka_checkout(self, branch_name: str):
        core_repo  =  Repo(os.path.join(self.root,  self.LAGARTO_KA_CORE))
        core_repo.remotes.origin.fetch()
        core_repo.git.checkout(branch_name)
        core_repo.remotes.origin.pull()
        core_repo.git.submodule('update', '--init', '--recursive')

    def lagarto_ka_commit_id(self):
        return Repo(self.lagarto_ka).head.object.hexsha

    def commit_id(self):
        return Repo(self.root).head.object.hexsha


    def lagarto_lowrisc_checkout(self, branch_name: str):
        repo =  Repo(self.root)
        repo.remotes.origin.fetch()
        repo.git.checkout(branch_name)
        repo.remotes.origin.pull()
        repo.git.submodule('update', '--init', '--recursive')


    def compile(self, log_file: typing.IO, nproc: int) -> subprocess.CompletedProcess:
        cmd = ['make', '-j' + str(nproc), 'all']
        return subprocess.run(cmd,
                              cwd     = self.vsim,
                              stdout  = log_file,
                              stderr  = subprocess.STDOUT) 


    def compile_benchmarks(self, log_file: typing.IO, nproc: int) -> subprocess.CompletedProcess:
        cmd = ['make', '-j' + str(nproc)]
        return subprocess.run(cmd,
                              cwd     = self.riscv_benchmarks,
                              stdout  = log_file,
                              stderr  = subprocess.STDOUT) 
